// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.2 (swiftlang-1103.0.32.1 clang-1103.0.32.29)
// swift-module-flags: -target arm64-apple-ios11.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Viewity
import Swift
import UIKit.UICollectionView
import UIKit.UIDevice
import UIKit.UIImageView
import UIKit
import UIKit.UIStackView
import UIKit.UIStoryboard
import UIKit.UITableView
import UIKit.UIView
@_exported import Viewity
extension UIStoryboard {
  public func bring<Controller>(_ type: Controller.Type) -> Controller where Controller : UIKit.UIViewController
}
extension UITableView {
  @discardableResult
  public func updateBatch(_ block: () -> Swift.Void) -> Self
  @discardableResult
  public func reload() -> Self
  @discardableResult
  public func dataSource(_ source: UIKit.UITableViewDataSource?) -> Self
  @discardableResult
  public func delegate(_ source: UIKit.UITableViewDelegate?) -> Self
  @discardableResult
  public func register<Cell>(_ type: Cell.Type) -> Self where Cell : UIKit.UITableViewCell
  public func dequeue<Cell>(_ type: Cell.Type, for indexPath: Foundation.IndexPath) -> Cell where Cell : UIKit.UITableViewCell
  @discardableResult
  public func setNoDataView(message: Swift.String, with icon: UIKit.UIImage?, and tintColor: UIKit.UIColor = .blue, for state: Swift.Bool) -> Self
}
extension UICollectionView {
  @discardableResult
  public func reload() -> Self
  @discardableResult
  public func dataSource(_ source: UIKit.UICollectionViewDataSource?) -> Self
  @discardableResult
  public func delegate(_ source: UIKit.UICollectionViewDelegate?) -> Self
  @discardableResult
  public func register<Cell>(_ type: Cell.Type) -> Self where Cell : UIKit.UICollectionViewCell
  public func dequeue<Cell>(_ type: Cell.Type, for indexPath: Foundation.IndexPath) -> Cell where Cell : UIKit.UICollectionViewCell
  public func dequeue<Cell>(_ type: Cell.Type, of kind: Swift.String, for indexPath: Foundation.IndexPath) -> Cell where Cell : UIKit.UICollectionReusableView
  @discardableResult
  public func setNoDataView(message: Swift.String, with icon: UIKit.UIImage?, and tintColor: UIKit.UIColor = .blue, for state: Swift.Bool) -> Self
}
extension UIView {
  public var midHeight: CoreGraphics.CGFloat {
    get
  }
}
extension UIView {
  @discardableResult
  public func add(_ views: UIKit.UIView...) -> Self
  @discardableResult
  public func fill(_ margin: UIKit.UIEdgeInsets = .zero, _ directionRespect: UIKit.UIView.RespectDirection = .respect) -> Self?
  @discardableResult
  public func putInCenter(_ margin: UIKit.UIView.AxisMargin = .zero) -> Self
  @discardableResult
  public func putInCenter(at axis: UIKit.NSLayoutConstraint.Axis, _ margin: CoreGraphics.CGFloat = 0.0) -> Self
  @discardableResult
  public func setDimensions(_ dimensions: CoreGraphics.CGSize) -> Self
  @discardableResult
  public func equal(a d1: UIKit.UIView.Dimensions, to d2: UIKit.NSLayoutDimension, _ accessories: UIKit.UIView.Accessories = .zero) -> Self
  @discardableResult
  public func align(a axis: UIKit.UIView.HorizontalAxis, to a: UIKit.NSLayoutXAxisAnchor, _ constant: CoreGraphics.CGFloat = 0.0) -> Self
  @discardableResult
  public func align(a axis: UIKit.UIView.VerticalAxis, to a: UIKit.NSLayoutYAxisAnchor, _ constant: CoreGraphics.CGFloat = 0.0) -> Self
  @discardableResult
  public func setDimensions(_ dimenType: UIKit.UIView.Dimensions, _ size: CoreGraphics.CGFloat) -> Self
}
extension UIStackView {
  @discardableResult
  public func addArranged(_ views: UIKit.UIView...) -> Self
  @discardableResult
  public func removeArranged(subview: UIKit.UIView) -> Self
  @discardableResult
  public func removeArranged(views: UIKit.UIView...) -> Self
  @discardableResult
  public func insert(subview: UIKit.UIView, at index: Swift.Int) -> Self
  @discardableResult
  public func axis(_ value: UIKit.NSLayoutConstraint.Axis) -> Self
  @discardableResult
  public func distribution(_ value: UIKit.UIStackView.Distribution) -> Self
  @discardableResult
  public func alignment(_ value: UIKit.UIStackView.Alignment) -> Self
  @discardableResult
  public func spacing(_ value: CoreGraphics.CGFloat) -> Self
}
extension UIImageView {
  @discardableResult
  public func image(_ value: UIKit.UIImage) -> Self
  @discardableResult
  public func content(mode: UIKit.UIView.ContentMode) -> Self
}
public protocol Cacheable {
}
extension Cacheable where Self : UIKit.UIImageView {
  public func loadImage(from link: Swift.String, hint: UIKit.UIImage? = .none, _ errorHandler: ((Swift.Error?) -> Swift.Void)? = nil)
}
extension UIDevice {
  public var fullModel: Swift.String {
    get
  }
}
extension UIView {
  @discardableResult
  public func corners(_ radius: CoreGraphics.CGFloat = 8, style: UIKit.UIView.CornerStyle = .all) -> Self
  @discardableResult
  public func border(width: CoreGraphics.CGFloat, color: UIKit.UIColor) -> Self
  @discardableResult
  public func shadow(offset: UIKit.UIOffset, color: UIKit.UIColor, radius: CoreGraphics.CGFloat, opacity: Swift.Float) -> Self
  @discardableResult
  public func background(_ color: UIKit.UIColor?, animated: Swift.Bool = true) -> Self
  @discardableResult
  public func tint(_ color: UIKit.UIColor) -> Self
  @discardableResult
  public func hidden(_ hide: Swift.Bool) -> Self
  @discardableResult
  public func opacity(_ value: CoreGraphics.CGFloat) -> Self
  @discardableResult
  public func userInteraction(enabled: Swift.Bool) -> Self
  @discardableResult
  public func semantic(content attribute: UIKit.UISemanticContentAttribute) -> Self
  @discardableResult
  public func frame(_ value: CoreGraphics.CGRect) -> Self
  @discardableResult
  public func bounds(_ value: CoreGraphics.CGRect) -> Self
  @discardableResult
  public func transform(_ value: CoreGraphics.CGAffineTransform) -> Self
}
extension UIButton {
  @discardableResult
  public func icon(_ image: UIKit.UIImage?, for state: UIKit.UIControl.State = .normal) -> Self
  @discardableResult
  public func text(_ title: Swift.String, for state: UIKit.UIControl.State = .normal) -> Self
  @discardableResult
  public func enabled(_ state: Swift.Bool) -> Self
  @discardableResult
  public func foreground(_ color: UIKit.UIColor?, for state: UIKit.UIControl.State = .normal) -> Self
  @discardableResult
  public func target(in container: Any?, action: ObjectiveC.Selector, state: UIKit.UIControl.Event = .touchUpInside) -> Self
  @discardableResult
  public func font(_ typeface: UIKit.UIFont) -> Self
  @discardableResult
  public func alignment(_ value: UIKit.NSTextAlignment) -> Self
  @discardableResult
  public func content(insets: UIKit.UIEdgeInsets) -> Self
  @discardableResult
  public func horizontalContent(alignment: UIKit.UIControl.ContentHorizontalAlignment) -> Self
  @discardableResult
  public func verticalContent(alignment: UIKit.UIControl.ContentVerticalAlignment) -> Self
  @discardableResult
  public func icon(insets: UIKit.UIEdgeInsets) -> Self
  @discardableResult
  public func text(insets: UIKit.UIEdgeInsets) -> Self
}
extension UILabel {
  @discardableResult
  public func text(_ text: Swift.String) -> Self
  @discardableResult
  public func font(_ typeface: UIKit.UIFont) -> Self
  @discardableResult
  public func foreground(_ color: UIKit.UIColor) -> Self
  @discardableResult
  public func alignment(_ align: UIKit.NSTextAlignment) -> Self
  @discardableResult
  public func number(ofLines value: Swift.Int) -> Self
  @discardableResult
  public func fitToWidth(_ scale: CoreGraphics.CGFloat = 0.5) -> Self
}
extension UITextField {
  @discardableResult
  public func placeholder(_ text: Swift.String) -> Self
  @discardableResult
  public func right(view: UIKit.UIView) -> Self
  @discardableResult
  public func right(mode: UIKit.UITextField.ViewMode) -> Self
  @discardableResult
  public func left(mode: UIKit.UITextField.ViewMode) -> Self
  @discardableResult
  public func left(view: UIKit.UIView) -> Self
  @discardableResult
  public func text(_ words: Swift.String) -> Self
  @discardableResult
  public func alignment(_ value: UIKit.NSTextAlignment) -> Self
  @discardableResult
  public func foreground(_ value: UIKit.UIColor) -> Self
  @discardableResult
  public func border(style: UIKit.UITextField.BorderStyle) -> Self
  @discardableResult
  public func font(_ typeface: UIKit.UIFont) -> Self
  @discardableResult
  public func enabled(_ force: Swift.Bool) -> Self
  @discardableResult
  public func unfocus() -> Self
  @discardableResult
  public func focus() -> Self
  @discardableResult
  public func delegate(_ value: UIKit.UITextFieldDelegate) -> Self
}
extension UITextView {
  @discardableResult
  public func unfocus() -> Self
  @discardableResult
  public func focus() -> Self
  @discardableResult
  public func font(_ typeface: UIKit.UIFont) -> Self
  @discardableResult
  public func editable(_ value: Swift.Bool) -> Self
  @discardableResult
  public func text(_ words: Swift.String) -> Self
  @discardableResult
  public func alignment(_ value: UIKit.NSTextAlignment) -> Self
  @discardableResult
  public func foreground(_ value: UIKit.UIColor) -> Self
  @discardableResult
  public func selectable(_ value: Swift.Bool) -> Self
  @discardableResult
  public func delegate(_ value: UIKit.UITextViewDelegate) -> Self
}
extension UIView {
  public typealias AxisMargin = CoreGraphics.CGPoint
  public enum Dimensions {
    case width, height
    public static func == (a: UIKit.UIView.Dimensions, b: UIKit.UIView.Dimensions) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public enum HorizontalAxis {
    case right, left, leading, trailing
    public static func == (a: UIKit.UIView.HorizontalAxis, b: UIKit.UIView.HorizontalAxis) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public enum VerticalAxis {
    case top, bottom
    public static func == (a: UIKit.UIView.VerticalAxis, b: UIKit.UIView.VerticalAxis) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public enum RespectDirection {
    case respect, unrespect
    public static func == (a: UIKit.UIView.RespectDirection, b: UIKit.UIView.RespectDirection) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public struct Accessories {
    public let multiplier: CoreGraphics.CGFloat
    public let constant: CoreGraphics.CGFloat
    public static var zero: UIKit.UIView.Accessories {
      get
    }
  }
  public enum CornerStyle {
    case all, up, down, right, left, upLeftDownRight, upRightDownLeft
    case upLeft, downLeft, upRight, downRight
    public static func == (a: UIKit.UIView.CornerStyle, b: UIKit.UIView.CornerStyle) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
}
public enum Light {
  case on, off
  public static func == (a: Viewity.Light, b: Viewity.Light) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public protocol Indicatorable {
}
extension Indicatorable where Self : UIKit.UIViewController {
  public func makeNetworkLoaderIn(turn: Viewity.Light)
}
extension UIKit.UIView.Dimensions : Swift.Equatable {}
extension UIKit.UIView.Dimensions : Swift.Hashable {}
extension UIKit.UIView.HorizontalAxis : Swift.Equatable {}
extension UIKit.UIView.HorizontalAxis : Swift.Hashable {}
extension UIKit.UIView.VerticalAxis : Swift.Equatable {}
extension UIKit.UIView.VerticalAxis : Swift.Hashable {}
extension UIKit.UIView.RespectDirection : Swift.Equatable {}
extension UIKit.UIView.RespectDirection : Swift.Hashable {}
extension UIKit.UIView.CornerStyle : Swift.Equatable {}
extension UIKit.UIView.CornerStyle : Swift.Hashable {}
extension Viewity.Light : Swift.Equatable {}
extension Viewity.Light : Swift.Hashable {}
